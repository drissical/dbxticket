<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @livewireStyles
        @bukStyles(true)
    </head>
    <body>
        Home Page
        <footer>
            <script src="{{ mix('js/app.js') }}" defer></script>
            @livewireScripts
            @bukScripts(true)
        </footer>
    </body>
</html>